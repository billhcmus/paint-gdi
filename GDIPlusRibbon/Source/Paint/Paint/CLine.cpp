#include "stdafx.h"
#include "CLine.h"


CLine::CLine()
{
}

CLine::CLine(Point startpos, Point endpos) : CShape(startpos, endpos)
{

}


CLine::~CLine()
{
}


void CLine::Draw(Graphics* graphics)
{
	graphics->DrawLine(pen, this->StartPos, this->EndPos);
}