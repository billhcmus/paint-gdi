// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
#include "stdafx.h"
#include <UIRibbon.h>
#include "resource.h"
#include "CommandHandler.h"
#include "RibbonIDs.h"
#include "RibbonFramework.h"
#include <UIRibbonPropertyHelpers.h>
// Static method to create an instance of the object.
HRESULT CCommandHandler::CreateInstance(IUICommandHandler **ppCommandHandler)
{
	if (!ppCommandHandler)
	{
		return E_POINTER;
	}

	*ppCommandHandler = NULL;

	HRESULT hr = S_OK;

	CCommandHandler* pCommandHandler = new CCommandHandler();

	if (pCommandHandler != NULL)
	{
		*ppCommandHandler = static_cast<IUICommandHandler *>(pCommandHandler);
	}
	else
	{
		hr = E_OUTOFMEMORY;
	}

	return hr;
}

// IUnknown method implementations.
STDMETHODIMP_(ULONG) CCommandHandler::AddRef()
{
	return InterlockedIncrement(&m_cRef);
}

STDMETHODIMP_(ULONG) CCommandHandler::Release()
{
	LONG cRef = InterlockedDecrement(&m_cRef);
	if (cRef == 0)
	{
		delete this;
	}

	return cRef;
}

STDMETHODIMP CCommandHandler::QueryInterface(REFIID iid, void** ppv)
{
	if (iid == __uuidof(IUnknown))
	{
		*ppv = static_cast<IUnknown*>(this);
	}
	else if (iid == __uuidof(IUICommandHandler))
	{
		*ppv = static_cast<IUICommandHandler*>(this);
	}
	else
	{
		*ppv = NULL;
		return E_NOINTERFACE;
	}

	AddRef();
	return S_OK;
}

//
//  FUNCTION: UpdateProperty()
//
//  PURPOSE: Called by the Ribbon framework when a command property (PKEY) needs to be updated.
//
//  COMMENTS:
//
//    This function is used to provide new command property values, such as labels, icons, or
//    tooltip information, when requested by the Ribbon framework.  
//    
//    In this SimpleRibbon sample, the method is not implemented.  
//
STDMETHODIMP CCommandHandler::UpdateProperty(
	UINT nCmdID,
	REFPROPERTYKEY key,
	const PROPVARIANT* ppropvarCurrentValue,
	PROPVARIANT* ppropvarNewValue)
{
	UNREFERENCED_PARAMETER(nCmdID);
	UNREFERENCED_PARAMETER(key);
	UNREFERENCED_PARAMETER(ppropvarCurrentValue);
	UNREFERENCED_PARAMETER(ppropvarNewValue);

	return E_NOTIMPL;
}

//
//  FUNCTION: Execute()
//
//  PURPOSE: Called by the Ribbon framework when a command is executed by the user.  For example, when
//           a button is pressed.
//
STDMETHODIMP CCommandHandler::Execute(
	UINT nCmdID,
	UI_EXECUTIONVERB verb,
	const PROPERTYKEY* key,
	const PROPVARIANT* ppropvarValue,
	IUISimplePropertySet* pCommandExecutionProperties)
{
	UNREFERENCED_PARAMETER(pCommandExecutionProperties);
	UNREFERENCED_PARAMETER(ppropvarValue);
	UNREFERENCED_PARAMETER(key);
	UNREFERENCED_PARAMETER(verb);
	UNREFERENCED_PARAMETER(nCmdID);

	HWND hWnd = GetForegroundWindow();
	PROPVARIANT var;
	switch (nCmdID)
	{
	case ID_CMD_LINE:
		
		g_pFramework->GetUICommandProperty(ID_CMD_LINE, UI_PKEY_BooleanValue, &var);
		var.boolVal = 0;
		g_pFramework->SetUICommandProperty(ID_CMD_RECT, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_ELLIPSE, UI_PKEY_BooleanValue, var);

		var.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_CMD_LINE, UI_PKEY_BooleanValue, var);
		break;
	case ID_CMD_RECT:
		g_pFramework->GetUICommandProperty(ID_CMD_RECT, UI_PKEY_BooleanValue, &var);
		var.boolVal = 0;
		g_pFramework->SetUICommandProperty(ID_CMD_LINE, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_ELLIPSE, UI_PKEY_BooleanValue, var);

		var.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_CMD_RECT, UI_PKEY_BooleanValue, var);
		break;
	case ID_CMD_ELLIPSE:
		g_pFramework->GetUICommandProperty(ID_CMD_ELLIPSE, UI_PKEY_BooleanValue, &var);
		var.boolVal = 0;
		g_pFramework->SetUICommandProperty(ID_CMD_LINE, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_RECT, UI_PKEY_BooleanValue, var);

		var.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_CMD_ELLIPSE, UI_PKEY_BooleanValue, var);
		break;
	case ID_CMD_1PX:
		g_pFramework->GetUICommandProperty(ID_CMD_1PX, UI_PKEY_BooleanValue, &var);
		var.boolVal = 0;
		g_pFramework->SetUICommandProperty(ID_CMD_3PX, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_5PX, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_8PX, UI_PKEY_BooleanValue, var);

		var.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_CMD_1PX, UI_PKEY_BooleanValue, var);
		break;
	case ID_CMD_3PX:
		g_pFramework->GetUICommandProperty(ID_CMD_3PX, UI_PKEY_BooleanValue, &var);
		var.boolVal = 0;
		g_pFramework->SetUICommandProperty(ID_CMD_1PX, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_5PX, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_8PX, UI_PKEY_BooleanValue, var);

		var.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_CMD_3PX, UI_PKEY_BooleanValue, var);
		break;
	case ID_CMD_5PX:
		g_pFramework->GetUICommandProperty(ID_CMD_5PX, UI_PKEY_BooleanValue, &var);
		var.boolVal = 0;
		g_pFramework->SetUICommandProperty(ID_CMD_3PX, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_1PX, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_8PX, UI_PKEY_BooleanValue, var);

		var.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_CMD_5PX, UI_PKEY_BooleanValue, var);
		break;
	case ID_CMD_8PX:
		g_pFramework->GetUICommandProperty(ID_CMD_8PX, UI_PKEY_BooleanValue, &var);
		var.boolVal = 0;
		g_pFramework->SetUICommandProperty(ID_CMD_3PX, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_1PX, UI_PKEY_BooleanValue, var);
		g_pFramework->SetUICommandProperty(ID_CMD_5PX, UI_PKEY_BooleanValue, var);

		var.boolVal = -1;
		g_pFramework->SetUICommandProperty(ID_CMD_8PX, UI_PKEY_BooleanValue, var);
		break;
	}
	PostMessage(hWnd, WM_COMMAND, nCmdID, 0);
	return S_OK;
}
