#include "stdafx.h"
#include "Shape.h"

CShape::CShape()
{
	this->pen = new Pen(Color(255, 0, 0, 0));
	this->brush = new SolidBrush(Color(255, 0, 0, 255));
}

CShape::CShape(Point startpos, Point endpos)
{

	this->StartPos = startpos;
	this->EndPos = endpos;
}

void CShape::SetStartPos(Point startpos)
{
	this->StartPos = startpos;
}

void CShape::SetEndPos(Point endpos)
{
	this->EndPos = endpos;
}

void CShape::SetSizePen(int lineWidth)
{
	this->pen->SetWidth(lineWidth);
}

void CShape::SetPenColor(DWORD rgb)
{
	Color clr;
	clr.SetFromCOLORREF(rgb);
	this->pen->SetColor(clr);
}