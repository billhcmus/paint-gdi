#pragma once
#include "Shape.h"
class CLine : public CShape
{
public:
	CLine();
	CLine(Point, Point);
	~CLine();
	virtual void Draw(Graphics *);
	CShape* Clone()
	{
		return new CLine;
	}
};

