﻿// Paint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Paint.h"
#include "CLine.h"
#include "CRectangle.h"
#include "CEllipse.h"
#include "CPNG.h"
#include "CBMP.h"
#include "Commdlg.h"
#include <fstream>
#include <windowsx.h>
#include <vector>
#include <gdiplusimagecodec.h>
#include <Objbase.h>
#include "RibbonFramework.h"
#include "RibbonIDs.h"

#pragma comment(lib, "Ole32.lib")


#define MAX_LOADSTRING 100
#define LINE 0
#define RECTANGLE 1
#define ELLIPSE 2

using namespace std;

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr))
	{
		return FALSE;
	}
	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_PAINT, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PAINT));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	CoUninitialize();
	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = 0; //CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PAINT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_PAINT);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
HMENU hMenu; // Menu

POINT point;
int x, y; // tọa độ con trỏ chuột
bool isDrawing = false; // đang vẽ hay không
Point StartPos, EndPos, CurPos; // điểm bắt đầu, điểm kết thúc, điểm hiện tại của con trỏ chuột
//CShape* shape;
vector<CShape*> shapes; // mảng chứa shape

// Tạo mẫu đối tượng
vector<CShape*> ShapePrototypes;
int shapeType = -1;

// Memory DC
HDC hdcMem;
HBITMAP hbmMem;
HANDLE hOld;

// Save, Open
OPENFILENAME ofn;
OPENFILENAME sfn;
const int BUFFER_SIZE = 255;
WCHAR szFileName[BUFFER_SIZE] = L"";
Graphics* graphics;
int lineWidth = 1;
DWORD rgb = 0;
/*
0: Line
1: Rectangle
2: Ellipse
*/
// tính toán tọa độ y để tạo hình vuông, tròn
/*

A * * * * C
   *    *
	 *  *
	    * B
A(startpos.x, startpos.y)
B(endpos.x, endpos.y)
C(endpos.x, startpos.y)
y : endpos.y thay đổi sao cho AC = CB
*/
int CalcY(Point startpos, Point endpos)
{
	int y;
	if (endpos.X > startpos.X)
	{
		if (endpos.Y > startpos.Y)
		{
			y = endpos.X - startpos.X + startpos.Y;
		}
		else
		{
			y = startpos.X + startpos.Y - endpos.X;
		}
	}
	else
	{
		if (endpos.Y > startpos.Y)
		{
			y = startpos.X - endpos.X + startpos.Y;
		}
		else
		{
			y = startpos.Y - startpos.X + endpos.X;
		}
	}
	return y;
}

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	Gdiplus::GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure

	Gdiplus::GetImageEncoders(num, size, pImageCodecInfo);

	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}

// tạo struct file bitmap (nguồn MSDN)
PBITMAPINFO CreateBitmapInfoStruct(HWND hwnd, HBITMAP hBmp)
{
	BITMAP bmp;
	PBITMAPINFO pbmi;
	WORD    cClrBits;

	// Retrieve the bitmap color format, width, and height.  
	if (!GetObject(hBmp, sizeof(BITMAP), (LPSTR)&bmp))
		MessageBox(hwnd, L"GetObject", L"Error", MB_OK);

	// Convert the color format to a count of bits.  
	cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel);
	if (cClrBits == 1)
		cClrBits = 1;
	else if (cClrBits <= 4)
		cClrBits = 4;
	else if (cClrBits <= 8)
		cClrBits = 8;
	else if (cClrBits <= 16)
		cClrBits = 16;
	else if (cClrBits <= 24)
		cClrBits = 24;
	else cClrBits = 32;

	// Allocate memory for the BITMAPINFO structure. (This structure  
	// contains a BITMAPINFOHEADER structure and an array of RGBQUAD  
	// data structures.)  

	if (cClrBits < 24)
		pbmi = (PBITMAPINFO)LocalAlloc(LPTR,
			sizeof(BITMAPINFOHEADER) +
			sizeof(RGBQUAD) * (1 << cClrBits));

	// There is no RGBQUAD array for these formats: 24-bit-per-pixel or 32-bit-per-pixel 

	else
		pbmi = (PBITMAPINFO)LocalAlloc(LPTR,
			sizeof(BITMAPINFOHEADER));

	// Initialize the fields in the BITMAPINFO structure.  

	pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pbmi->bmiHeader.biWidth = bmp.bmWidth;
	pbmi->bmiHeader.biHeight = bmp.bmHeight;
	pbmi->bmiHeader.biPlanes = bmp.bmPlanes;
	pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel;
	if (cClrBits < 24)
		pbmi->bmiHeader.biClrUsed = (1 << cClrBits);

	// If the bitmap is not compressed, set the BI_RGB flag.  
	pbmi->bmiHeader.biCompression = BI_RGB;

	// Compute the number of bytes in the array of color  
	// indices and store the result in biSizeImage.  
	// The width must be DWORD aligned unless the bitmap is RLE 
	// compressed. 
	pbmi->bmiHeader.biSizeImage = ((pbmi->bmiHeader.biWidth * cClrBits + 31) & ~31) / 8
		* pbmi->bmiHeader.biHeight;
	// Set biClrImportant to 0, indicating that all of the  
	// device colors are important.  
	pbmi->bmiHeader.biClrImportant = 0;
	return pbmi;
}

// tạo file bitmap
void CreateBMPFile(HWND hwnd, LPTSTR pszFile, PBITMAPINFO pbi,
	HBITMAP hBMP, HDC hDC)
{
	HANDLE hf;                 // file handle  
	BITMAPFILEHEADER hdr;       // bitmap file-header  
	PBITMAPINFOHEADER pbih;     // bitmap info-header  
	LPBYTE lpBits;              // memory pointer  
	DWORD dwTotal;              // total count of bytes  
	DWORD cb;                   // incremental count of bytes  
	BYTE *hp;                   // byte pointer  
	DWORD dwTmp;

	pbih = (PBITMAPINFOHEADER)pbi;
	lpBits = (LPBYTE)GlobalAlloc(GMEM_FIXED, pbih->biSizeImage);

	if (!lpBits)
		MessageBox(hwnd, L"GlobalAlloc", L"Error", MB_OK);

	// Retrieve the color table (RGBQUAD array) and the bits  
	// (array of palette indices) from the DIB.  
	if (!GetDIBits(hDC, hBMP, 0, (WORD)pbih->biHeight, lpBits, pbi,
		DIB_RGB_COLORS))
	{
		MessageBox(hwnd, L"GetDIBits", L"Error", MB_OK);
	}

	// Create the .BMP file.  
	hf = CreateFile(pszFile,
		GENERIC_READ | GENERIC_WRITE,
		(DWORD)0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		(HANDLE)NULL);
	if (hf == INVALID_HANDLE_VALUE)
		MessageBox(hwnd, L"CreateFile", L"Error", MB_OK);
	hdr.bfType = 0x4d42;        // 0x42 = "B" 0x4d = "M"  
								// Compute the size of the entire file.  
	hdr.bfSize = (DWORD)(sizeof(BITMAPFILEHEADER) +
		pbih->biSize + pbih->biClrUsed
		* sizeof(RGBQUAD) + pbih->biSizeImage);
	hdr.bfReserved1 = 0;
	hdr.bfReserved2 = 0;

	// Compute the offset to the array of color indices.  
	hdr.bfOffBits = (DWORD) sizeof(BITMAPFILEHEADER) +
		pbih->biSize + pbih->biClrUsed
		* sizeof(RGBQUAD);

	// Copy the BITMAPFILEHEADER into the .BMP file.  
	if (!WriteFile(hf, (LPVOID)&hdr, sizeof(BITMAPFILEHEADER),
		(LPDWORD)&dwTmp, NULL))
	{
		MessageBox(hwnd, L"WriteFile", L"Error", MB_OK);
	}

	// Copy the BITMAPINFOHEADER and RGBQUAD array into the file.  
	if (!WriteFile(hf, (LPVOID)pbih, sizeof(BITMAPINFOHEADER)
		+ pbih->biClrUsed * sizeof(RGBQUAD),
		(LPDWORD)&dwTmp, (NULL)))
		MessageBox(hwnd, L"WriteFile", L"Error", MB_OK);

	// Copy the array of color indices into the .BMP file.  
	dwTotal = cb = pbih->biSizeImage;
	hp = lpBits;
	if (!WriteFile(hf, (LPSTR)hp, (int)cb, (LPDWORD)&dwTmp, NULL))
		MessageBox(hwnd, L"WriteFile", L"Error", MB_OK);

	// Close the .BMP file.  
	if (!CloseHandle(hf))
		MessageBox(hwnd, L"CloseHandle", L"Error", MB_OK);

	// Free memory.  
	GlobalFree((HGLOBAL)lpBits);
}


GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR gdiplusToken;
bool initSuccess;
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	hMenu = GetMenu(hWnd);

	switch (message)
	{
	case WM_CREATE:
	{
		// Init ribbon framework
		initSuccess = InitializeFramework(hWnd);
		if (!initSuccess)
		{
			return -1;
		}
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		ShapePrototypes.push_back(new CLine);
		ShapePrototypes.push_back(new CRectangle);
		ShapePrototypes.push_back(new CEllipse);
		break;
	}
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case ID_CMD_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_CMD_NEW:
			for (int i = shapes.size() - 1; i >= 0; i--)
			{
				delete shapes[i];
				shapes.pop_back();
			}
			InvalidateRect(hWnd, NULL, FALSE);
			break;
		case ID_CMD_OPEN:
			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = hWnd;
			ofn.nMaxFile = BUFFER_SIZE;
			ofn.lpstrFilter = L"BMP (*.bmp) \0*.bmp\0";
			ofn.lpstrFile = szFileName;
			ofn.nFilterIndex = 1;
			ofn.lpstrInitialDir = NULL;
			ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;
			if(GetOpenFileName(&ofn) == TRUE)
			{
				Image* img = Image::FromFile(ofn.lpstrFile);
				Point p;
				p.X = 0;
				p.Y = GetRibbonHeight();
				CBMP* bmpImg = new CBMP(img, p);
				for (int i = shapes.size() - 1; i >= 0; i--)
				{
					delete shapes[i];
					shapes.pop_back();
				}
				shapes.push_back(bmpImg);
				InvalidateRect(hWnd, NULL, FALSE);
			}
			break;
		case ID_CMD_SAVE: // lưu file bitmap
			DWORD fileAttr;
			fileAttr = GetFileAttributes(szFileName);
			if (fileAttr == 0xFFFFFFFF) { // Chưa có file nào có sẵn
				ZeroMemory(&sfn, sizeof(sfn)); 
				sfn.lStructSize = sizeof(sfn);
				sfn.hwndOwner = hWnd;
				sfn.nMaxFile = BUFFER_SIZE;
				sfn.lpstrFilter = L"BMP (*.bmp) \0*.bmp\0";
				sfn.lpstrFile = szFileName;
				sfn.nFilterIndex = 1;
				sfn.lpstrInitialDir = NULL;
				sfn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT;
				sfn.lpstrDefExt = L"File";
				if (GetSaveFileName(&sfn)) // lấy tên file
				{
					HDC hdc = CreateCompatibleDC(GetDC(hWnd));

					RECT rect;
					GetClientRect(hWnd, &rect);
					HBITMAP hbmScreen = CreateCompatibleBitmap(GetDC(hWnd), rect.right, rect.bottom);
					SelectObject(hdc, hbmScreen);
					FillRect(hdc, &rect, (HBRUSH)(COLOR_WINDOW + 1));
					BitBlt(hdc, 0, GetRibbonHeight(), rect.right, rect.bottom, GetDC(hWnd), 0, GetRibbonHeight(), SRCCOPY);

					PBITMAPINFO pbi = CreateBitmapInfoStruct(hWnd, hbmScreen);
					CreateBMPFile(hWnd, sfn.lpstrFile, pbi, hbmScreen, GetDC(hWnd));
				}
			}
			else
			{
				HDC hdc = CreateCompatibleDC(GetDC(hWnd));

				RECT rect;
				GetClientRect(hWnd, &rect);
				HBITMAP hbmScreen = CreateCompatibleBitmap(GetDC(hWnd), rect.right, rect.bottom);
				SelectObject(hdc, hbmScreen);
				FillRect(hdc, &rect, (HBRUSH)(COLOR_WINDOW + 1));
				BitBlt(hdc, 0, 0, rect.right, rect.bottom, GetDC(hWnd), 0, 0, SRCCOPY);

				PBITMAPINFO pbi = CreateBitmapInfoStruct(hWnd, hbmScreen);
				CreateBMPFile(hWnd, szFileName, pbi, hbmScreen, GetDC(hWnd));
			}

			
			break;
		case ID_CMD_LINE:
			//CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED);

			//// uncheck mục còn lại
			//CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
			//CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
			shapeType = LINE;
			break;
		case ID_CMD_RECT:
			/*CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_CHECKED);

			CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED);
			CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);*/
			shapeType = RECTANGLE;
			break;
		case ID_CMD_ELLIPSE:
			/*CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_CHECKED);

			CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
			CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED);*/
			shapeType = ELLIPSE;
			break;
		case ID_CMD_1PX:
			lineWidth = 1;
			break;
		case ID_CMD_3PX:
			lineWidth = 3;
			break;
		case ID_CMD_5PX:
			lineWidth = 5;
			break;
		case ID_CMD_8PX:
			lineWidth = 8;
			break;
		case ID_CMD_CHOOSE_COLOR:
			CHOOSECOLOR cc;                 // common dialog box structure 
			static COLORREF acrCustClr[16]; // array of custom colors 
											// owner window
			HBRUSH hbrush;                  // brush handle
			static DWORD rgbCurrent;        // initial color selection

											// Initialize CHOOSECOLOR 
			ZeroMemory(&cc, sizeof(cc));
			cc.lStructSize = sizeof(cc);
			cc.hwndOwner = hWnd;
			cc.lpCustColors = (LPDWORD)acrCustClr;
			cc.rgbResult = rgbCurrent;
			cc.Flags = CC_FULLOPEN | CC_RGBINIT;

			if (ChooseColor(&cc) == TRUE)
			{
				hbrush = CreateSolidBrush(cc.rgbResult);
				rgbCurrent = cc.rgbResult;
				rgb = rgbCurrent;
			}

			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_LBUTTONDOWN:
	{
		x = GET_X_LPARAM(lParam);
		y = GET_Y_LPARAM(lParam);
		if (!isDrawing && shapeType != -1)
		{
			shapes.push_back(ShapePrototypes[shapeType]->Clone());
			isDrawing = true;
			StartPos.X = x;
			StartPos.Y = y;
			shapes[shapes.size() - 1]->SetStartPos(StartPos);
			shapes[shapes.size() - 1]->SetSizePen(lineWidth);
			shapes[shapes.size() - 1]->SetPenColor(rgb);
		//	HBRUSH hbrush = GetStockBrush(NULL_BRUSH);
		//	shapes[shapes.size() - 1]->SetBrush(hbrush);
			SetCapture(hWnd); // cho chuot ra khoi cua so
		}
		break;
	}
	break;
	case WM_MOUSEMOVE:
	{
		if (isDrawing == true)
		{
			x = GET_X_LPARAM(lParam);
			y = GET_Y_LPARAM(lParam);
			CurPos.X = x, CurPos.Y = y;
			TCHAR buffer[10];
			wsprintf(buffer, L"%d, %d", x, y);
			SetWindowText(hWnd, buffer);
			if (MK_SHIFT&wParam && (shapeType == RECTANGLE || shapeType == ELLIPSE)) // nếu nhấn shift và đó là hình chữ nhật hoặc hình ellipse
			{
				CurPos.Y = CalcY(StartPos, CurPos);
			}
			int index = shapes.size();
			shapes[index - 1]->SetEndPos(CurPos);
			InvalidateRect(hWnd, NULL, FALSE);
		}
	}
	break;
	case WM_LBUTTONUP:
	{
		if (isDrawing == true)
		{
			isDrawing = false; // hết vẽ
							   // Lấy điểm kết thúc
			EndPos.X = GET_X_LPARAM(lParam);
			EndPos.Y = GET_Y_LPARAM(lParam);
			if (MK_SHIFT&wParam && (shapeType == RECTANGLE || shapeType == ELLIPSE))
			{
				EndPos.Y = CalcY(StartPos, EndPos);
			}
			shapes[shapes.size() - 1]->SetEndPos(EndPos);
			ReleaseCapture();
			InvalidateRect(hWnd, NULL, FALSE);
		}
		break;
	}
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		RECT rect;
		HDC hdc = BeginPaint(hWnd, &ps);
		HPALETTE hPalette;
		BITMAP bm;

		GetClientRect(hWnd, &rect);
		// TODO: Add any drawing code that uses hdc here...
		hdcMem = CreateCompatibleDC(hdc);
		hbmMem = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
		hOld = SelectObject(hdcMem, hbmMem); // lưu lại
		FillRect(hdcMem, &rect, (HBRUSH)(COLOR_WINDOW + 1));
		graphics = new Graphics(hdcMem);
		for (int i = 0; i < shapes.size(); i++)
		{
			shapes[i]->Draw(graphics);
		}

		BitBlt(hdc, 0, GetRibbonHeight(), rect.right, rect.bottom, hdcMem, 0,  GetRibbonHeight(), SRCCOPY);
		SelectObject(hdcMem, hOld); // lấy lên
		DeleteObject(hbmMem); // giải phóng
		DeleteDC(hdcMem);
		EndPaint(hWnd, &ps);
	}
	break;

	case WM_DESTROY:
		GdiplusShutdown(gdiplusToken);
		DestroyFramework();
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
