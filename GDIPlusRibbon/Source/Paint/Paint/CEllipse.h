#pragma once
#include "Shape.h"
class CEllipse : public CShape
{
public:
	CEllipse();
	CEllipse(Point, Point);
	~CEllipse();
	virtual void Draw(Graphics*);
	CShape* Clone()
	{
		return new CEllipse;
	}
};

