#include "stdafx.h"
#include "CRectangle.h"


CRectangle::CRectangle()
{
}

CRectangle::CRectangle(Point startpos, Point endpos) : CShape(startpos, endpos)
{

}

CRectangle::~CRectangle()
{
}

void CRectangle::Draw(Graphics* graphics)
{
	graphics->DrawRectangle(pen, this->StartPos.X, this->StartPos.Y, this->EndPos.X - this->StartPos.X, this->EndPos.Y - this->StartPos.Y);
}