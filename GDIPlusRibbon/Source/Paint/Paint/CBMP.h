#pragma once
#include "Shape.h"
class CBMP :
	public CShape
{
private:
	Image* im;
	Point point;
public:
	void Draw(Graphics*);
	CShape* Clone()
	{
		return NULL;
	}
	CBMP();
	CBMP(Image*, Point);
	~CBMP();
};

