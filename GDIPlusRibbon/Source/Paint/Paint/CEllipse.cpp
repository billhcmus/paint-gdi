#include "stdafx.h"
#include "CEllipse.h"


CEllipse::CEllipse()
{
}


CEllipse::~CEllipse()
{
}

CEllipse::CEllipse(Point startpos, Point endpos) : CShape(startpos, endpos)
{

}

void CEllipse::Draw(Graphics* graphics)
{
	graphics->DrawEllipse(pen, this->StartPos.X, this->StartPos.Y, this->EndPos.X - this->StartPos.X, this->EndPos.Y - this->StartPos.Y);
}