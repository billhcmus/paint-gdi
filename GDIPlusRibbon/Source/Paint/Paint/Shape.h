#pragma once
#include "resource.h"
#include <objidl.h>
#include <gdiplus.h>
#pragma comment (lib, "Gdiplus.lib")
using namespace Gdiplus;

class CShape
{
protected:
	Point StartPos, EndPos;
	Pen *pen;
	Brush *brush;
public:
	void SetStartPos(Point);
	void SetEndPos(Point);
	void SetSizePen(int);
	void SetPenColor(DWORD);
	CShape();
	CShape(Point, Point);
	virtual void Draw(Graphics*) = 0;
	virtual CShape* Clone() = 0;
};