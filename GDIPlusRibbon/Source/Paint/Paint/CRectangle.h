#pragma once
#include "Shape.h"
class CRectangle : public CShape
{
public:
	CRectangle();
	CRectangle(Point, Point);
	virtual void Draw(Graphics*);
	CShape* Clone()
	{
		return new CRectangle;
	}
	~CRectangle();
};

